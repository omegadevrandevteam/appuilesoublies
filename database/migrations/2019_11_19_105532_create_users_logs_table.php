<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_logs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->char('association_name');
            $table->char('first_name');
            $table->char('last_name');
            $table->char('address');
            $table->char('ville');
            $table->char('email')->unique();
            $table->boolean('is_activated')->default(0);
            $table->dateTime('email_verified_at');
            $table->bigInteger('contact');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_logs');
    }
}
