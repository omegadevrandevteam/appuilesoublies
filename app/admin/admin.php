<?php

namespace App\admin;

use Illuminate\Database\Eloquent\Model;

class admin extends Model
{
    // table name
    protected $table = 'users_logs';
    // primary key
    public $primaryKey = 'id';
    // timestamps
    public $timestamps = true;

}
