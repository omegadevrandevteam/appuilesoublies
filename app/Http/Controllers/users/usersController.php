<?php

namespace App\Http\Controllers\Users;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use App\admin\admin;
use App\Mail\sendMail;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use Auth;
use DB;

class usersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){

        if(Auth::check()) {
            $users_list = admin::where('is_activated','1')->first()();
            return view('admin.list')->with('users_list', $users_list);
        }else {
            return redirect('/login');
        }
    }

    public function update_logo($token){
        $users_list = admin::where('auth_id',$token)->get();
        return view('users.update_logo',['users_list' => $users_list]);
    }

    public function save_logo(Request $request){
      define('DS', DIRECTORY_SEPARATOR);
      $id = $request->get('id');
      $file_logo  = $request->file('logo');
      $new_file_name = rand().'.'.$file_logo->getClientOriginalExtension();
        $path = public_path('images').DS.'logo'.DS.$id;
        $dirs = explode(DS, $path.DS);
        $url = '';
        foreach( $dirs as  $dir )
        {   $url .= DS.$dir;
            if (!is_dir($url)) {
                File::makeDirectory($url, 0777,true);
            }
        }
        $file_logo->move($url.DS, $new_file_name);
//        $users_list = admin::where('id',$id)->first()();

        $users = admin::where([
            'id' => $id
        ]);
        $users->update([
            'logo_image_name' => $new_file_name
        ]);
        echo "Logo enregistré avec succèss";
    }

}
