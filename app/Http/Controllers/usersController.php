<?php

namespace App\Http\Controllers\users;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use App\admin\admin;
use App\Mail\sendMail;
use Illuminate\Support\Facades\Mail;
use Auth;
use DB;

class usersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){

        if(Auth::check()) {
            $users_list = admin::where('is_activated','1')->get();
            return view('admin.list')->with('users_list', $users_list);
        }else {
            return redirect('/login');
        }
    }

    public function update_logo($token){
        $users_list = admin::where('auth_id',$token)->get();
        return view('users.update_logo')->with('users_list', $users_list);
    }
}
