<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use App\admin\admin;
use App\Mail\sendMail;
use Illuminate\Support\Facades\Mail;
use Auth;
use DB;

class adminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){

        if(Auth::check()) {
            $users_list = admin::where('is_activated','1')->get();
            return view('admin.list')->with('users_list', $users_list);
        }else {
            return redirect('/login');
        }
    }
    public function usrNotConfirmed(){
        if(Auth::check()) {
            $users_list = admin::where('is_activated','0')->get();
            return view('admin.unconf')->with('users_list', $users_list);
        }else {
            return redirect('/login');
        }
    }



    public function register($id){

        $info_user = admin::find($id);
        $longueur = 100;
        $caracteres = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $longueurMax = strlen($caracteres);
        $chaineAleatoire = '';
        for ($i = 0; $i < $longueur; $i++)
        {
            $chaineAleatoire .= $caracteres[rand(0, $longueurMax - 1)];
        }
        $token = $chaineAleatoire;
        $to_update = array(
            "is_activated" =>'1',
            "auth_id" =>$token,
        );

        $details = [
            'title' => 'Validation de votre inscription chez soutien.lesoubliesdelanation.fr',
            'body' => "Votre démande à été validé par l'administrateur",
            "mailUser" => $info_user->email,
            "token" =>$token
        ];
        Mail::to($info_user->email)->send(new \App\Mail\sendMail($details));
            DB::table('users_logs')
                ->where('id', $id)
                ->update($to_update);
            return redirect('/unconfusers')->with('message',"Mail de confirmation envoyé");

    }


    public function activationUsers($token){
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'Nom_association' =>'required',
            'Adresse' => 'required',
            'Ville' => 'required',
            'Nom' => 'required',
            'Prenom' => 'required',
            'Email' => 'required',
            'Contact' => 'required'
        ]);
        $is_exist = admin::where([
        'email' => $request->input('Email')
        ])->get();
        if (count($is_exist) > 0){

            echo "L'email existe déjà";

        }else{
            // create post

            $users = new admin();
            $users->association_name = $request->input('Nom_association');
            $users->address = $request->input('Adresse');
            $users->ville = $request->input('Ville');
            $users->first_name = $request->input('Nom');
            $users->last_name = $request->input('Prenom');
            $users->email = $request->input('Email');
            $users->contact = $request->input('Contact');
            $users->save();
            $details = [
                'title' => 'Demande d\' inscription chez soutien.lesoubliesdelanation.fr',
                'body' => "Une demande d'inscription sur soutien.lesoubliesdelanation.fr par ".$request->input('Email'),
                "mailUser" => $request->input('Email'),
            ];
            Mail::to("alphadev@randevteam.com")->send(new \App\Mail\sendMail($details));
            return redirect('/')->with('success', "Demande envoyé, Vous recevrez un Email de confirmation quand l'administrateur aura accepté votre demande");
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $show_user= admin::find($id);
        return view('admin.show')->with('show_user', $show_user);
    }
    public function shownonconf()
    {
    return view('admin.home');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Auth::check()) {
            $edit_user = admin::find($id);
            return view('homefrontController.edit')->with('edit_user', $edit_user);
        } else {
            return redirect('/login');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'Nom_association' =>'required',
            'Adresse' => 'required',
            'Ville' => 'required',
            'Nom' => 'required',
            'Prenom' => 'required',
            'Email' => 'required',
            'Contact' => 'required'
        ]);
        // create post

        $users = admin::find($id);
        $users->association_name = $request->input('Nom_association');
        $users->address = $request->input('Adresse');
        $users->ville = $request->input('Ville');
        $users->first_name = $request->input('Nom');
        $users->last_name = $request->input('Prenom');
        $users->email = $request->input('Email');
        $users->contact = $request->input('Contact');
        $users->save();
        return redirect('/admin')->with('success', 'Users Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $users = admin::find($id);
        $users->delete();
        return redirect('/admin')->with('success', 'Users removed');
    }



    public function homeroute(){


        if(Auth::check()) {
            return view('admin.home');
        }else {
            return redirect('/login');
        }

    }
}
