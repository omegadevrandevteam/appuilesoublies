@extends('layout.app')

@section('content')

    <div class="form-group">
        <div class="form-signin">
            <div class="text-center mb-4">
                <h1 class="h3 mb-3 font-weight-normal">Formulaire</h1>
                <p>Veuillez remplir le formulaire ci-dessous</p>
            </div>
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
    {!! Form::open(['action' => 'admin\adminController@store','method'=>'POST']) !!}
            <div class="form-label-group">
                {{Form::text('Nom_association', '', ['class' => 'form-control', 'placeholder' => 'Nom_association'])}}
                {{Form::label('title', 'Nom de L\'association',['for'=> 'Nom_association'])}}
            </div>
            <div class="form-label-group">
                {{Form::text('Adresse', '', ['class' => 'form-control', 'placeholder' => 'Adresse','required'])}}
                {{Form::label('title', 'Adresse',['for'=> 'Adresse'])}}
            </div>
            <div class="form-label-group">
                {{Form::text('Ville', '', ['class' => 'form-control', 'placeholder' => 'try','required'])}}
                {{Form::label('title', 'Ville',['for'=> 'Ville'])}}
            </div>
            <div class="form-label-group">
                {{Form::text('Nom', '', ['class' => 'form-control', 'placeholder' => 'Nom','required'])}}
                {{Form::label('title', 'Nom',['for'=> 'Nom'])}}
            </div>
            <div class="form-label-group">
                {{Form::text('Prenom', '', ['class' => 'form-control', 'placeholder' => 'Prenom','required'])}}
                {{Form::label('title','Prenom',['for'=> 'Prenom'])}}
            </div>
            <div class="form-label-group">
                {{Form::email('Email', '', ['class' => 'form-control', 'placeholder' => 'Email','required'])}}
                {{Form::label('title', 'Email',['for'=> 'Email'])}}
            </div>
            <div class="form-label-group">
                {{Form::number('Contact', '', ['class' => 'form-control', 'placeholder' => 'Contact','required'])}}
                {{Form::label('title', 'Contact',['for'=> 'Contact'])}}
            </div>
            {{Form::submit('Validate', ['class' =>'btn btn-block btn-lg btn-primary'])}}
            {{--<p class="mt-5 mb-3 text-muted text-center">&copy;by Zo 2019</p>--}}
    {!! Form::close() !!}
        </div>
        </div>
    </div>
@endsection
