
<!doctype html>

<html lang = '{{ config('app.locale') }}'>

<head>
    <meta  charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=devise-width, initial-scale=1">
    <link rel="stylesheet" href="{{asset('css/bootstrap-grid.css')}}">
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <title>{{config('app.name', 'LSAPP')}}</title>

</head>
<body>

@include('inc.usersnav')
<div class="container">
    @yield('content_users')
</div>
<script src="{{asset('js/bootstrap.js')}}"></script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</body>
</html>
