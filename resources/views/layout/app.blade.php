
<!doctype html>

<html lang = '{{ config('app.locale') }}'>

<head>

    <meta  charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=devise-width, initial-scale=1">
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('css/bootstrap-grid.css')}}">
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link rel="icon" type="image/png"  href="{{asset('image/email.png')}}"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css"/>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css"/>

    <title>{{config('app.name', 'LSAPP')}}</title>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#user_list').DataTable();
        } );
    </script>
</head>
<body>

    <div class="container">
        @include('inc.message')
    @yield('content')
    </div>
</body>
</html>
