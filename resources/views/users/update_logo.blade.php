@extends('layout.users_head')

@section('content_users')

    <div class="card">
        {{--<div class="card-header">--}}
            {{--Liste de Utilisateur--}}
        {{--</div>--}}
        <div class="card-body">
            <h5 class="card-title">Choisissez votre logo</h5>
        <form enctype="multipart/form-data" method="post" action="{{route('save_logo')}}">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <input type="hidden" name="id" value="{{$users_list[0]->id}}">
            <input type="file" name="logo" class="form-control">
            <input type="submit" class="form-control" value="Valider">
        </form>
        </div>
    </div>

@endsection
