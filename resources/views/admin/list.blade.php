@extends('layout.admin')

@section('content')



            <div class="card">
                <div class="card-header">
                    Liste de Utilisateur
                </div>
                <div class="card-body">
                    <h5 class="card-title">Utilisateurs confirmés</h5>

                    <table id="user_list" class="table table-hover">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col">Nom de la companie</th>
                            <th scope="col">Logo</th>
                            <th scope="col">Nom</th>
                            <th scope="col">contact</th>
                            <th scope="col">Email</th>
                            <th scope="col">Supprimer</th>
                        </tr>
                        </thead>


                        <tbody>
                        @if(count($users_list) > 0)
                            @foreach($users_list as $u_list)
                        <tr>
                            <th>{{$u_list->association_name}}</th>
                            @if( $u_list->logo_image_name != null && $u_list->logo_image_name !="" )
                            <td><img width="50px" height="auto" src="{{asset('public/images/logo')}}/{{$u_list->id}}/{{$u_list->logo_image_name}}"></td>
                            @else
                                <td>Pas de logo</td>
                            @endif
                                <td>{{$u_list->first_name}}</td>
                            <td>{{$u_list->contact}}</td>
                            <td>{{$u_list->email}}</td>
                            {{--<td>--}}
                                {{--<a href="/admin/{{$u_list->id}}" class="btn btn-primary">View</a>--}}
                                {{--<a href="/admin/{{$u_list->id}}/edit" class="btn btn-success">Modifier</a>--}}

                            {{--</td>--}}
                            <td>
                                {!! Form::open(['action' => ['admin\adminController@destroy' , $u_list->id],'method'=>'POST', 'class' => 'rw-inline']) !!}
                                {{Form::hidden('_method', 'DELETE')}}
                                <button class="btn btn-danger">Supprimer</button>
                                {!! Form::close() !!}
                            </td>
                        </tr>
                        @endforeach
                        @else
                            <tr><td><p>Pas de donnée</p></td></tr>
                        @endif
                        </tbody>

                    </table>

                </div>
            </div>

    </div>
            <script type="text/javascript">
                $(document).ready(function() {
                    $('#user_list').DataTable();
                } );
            </script>
@endsection

