@extends('layout.admin')

@section('content')



    <div class="card">
        <div class="card-header">
            Liste de Utilisateur
        </div>
        <div class="card-body">
            <h5 class="card-title">comfirmer et non comfirmet</h5>
            @if (session('message'))
                <div class="alert alert-success">
                    {{ session('message') }}
                </div>
            @endif
            <table class="table table-hover">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">Nom de la companie</th>
                    <th scope="col">Nom</th>
                    <th scope="col">contact</th>
                    <th scope="col">Email</th>
                    <th scope="col"></th>
                </tr>
                </thead>


                <tbody>
                @if(count($users_list) > 0)
                    @foreach($users_list as $u_list)
                        <tr>
                            <th>{{$u_list->association_name}}</th>
                            <td>{{$u_list->first_name}}</td>
                            <td>{{$u_list->contact}}</td>
                            <td>
                                {{$u_list->email}}
                                {{--<a href="/admin/{{$u_list->id}}" class="btn btn-primary">View</a>--}}
                                {{--<a href="/admin/{{$u_list->id}}/edit" class="btn btn-success">Edit</a>--}}

                            </td>
                            <td>
                                <a href="{{ route('send_confirm_mail', ['id' => $u_list->id]) }}"><button class="btn btn-primary">Confirmer</button></a>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr><td><p> no users found</p></td></tr>
                @endif
                </tbody>

            </table>

        </div>
    </div>

    </div>

@endsection
