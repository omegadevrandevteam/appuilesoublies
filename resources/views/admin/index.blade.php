@extends('layout.admin')

@section('content')

    @if(count($users_list) > 1)
        @foreach($users_list as $u_list)
            <div class="well">
                <a href="/admin/{{$u_list->id}}"><h3>{{$u_list->association_name}}</h3></a>
                <small> localised at :{{$u_list->address}}</small>
            </div>
        @endforeach
    @else
        <p> no users found</p>
    @endif

@endsection
