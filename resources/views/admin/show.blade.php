
@extends('layout.admin')

@section('content')
    <div class="col-md-5">
    <h1>{{$show_user->association_name}}</h1>
    </div>
    <div class="col-md-7">
    <small> user first name {{$show_user->first_name}}</small>
    <small> user last name {{$show_user->last_name}}</small>
    </div>
@endsection
