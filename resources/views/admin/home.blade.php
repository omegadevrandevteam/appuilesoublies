@extends('layout.admin')

@section('content')

<main role="main">

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
        <div class="container">
            <h1 class="display-3">Bonjour ,{{ Auth::user()->name }} !</h1>
            <p>Ici tu peut gerer les utilisateur du site !!</p>
        </div>
    </div>

    <div class="container">
        <!-- Example row of columns -->
        <div class="row">
            <div class="col-md-6 h-100 hover_style_left">
                <h2 class="text-center">Liste des Utilisateur</h2>
                <p class="text-center"><a class="btn btn-secondary" href="/admin" role="button">Voir la liste &raquo;</a></p>
            </div>
            <div class="col-md-6 h-100 hover_style_right">
                <h2 class="text-center">Liste des Utilisateurs non confirmés</h2>
                <p></p>
                <p class="text-center"><a class="btn btn-secondary" href="/unconfusers" role="button">Voir la liste &raquo;</a></p>
            </div>
            {{--<div class="col-md-4 h-100">--}}
                {{--<h2>Mon compte</h2>--}}
                {{--<p>Gerer mon compte </p>--}}
                {{--<p><a class="btn btn-secondary" href="#" role="button">Plus de details</a></p>--}}
            {{--</div>--}}
        </div>

        <hr>

    </div> <!-- /container -->

</main>

@endsection
