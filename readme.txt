database name : simple_login

route :: 

route for admin pannel  : localhoost:8000/admin ( login requise)

			- 
route for main app	: localhost:8000

===============================================================================
controller::

main controller : controller/admin/adminController.php 
		function :

			- index     		: affiche la liste des utilisateur 
			- usrNotConfirmed	: affiche les utilisateur non confirmet 
			- store			: ajoute une nouvelle utilisateur
			- edit & update 	: editer les utilisateur existant
			- destroy		: suprimme un utilisateur
			- register 		: envoy d'email (non fonctionnelle)


===============================================================================
view ::

navbar : views/inc/navbar.blade.php


layouts: 
		- for admin pannel:
					views/layout/admin.blade.php
		- for main app     :
					views/layout/app.blade.php

pages : 

		-admin : 
			home of users     :
					views/admin/home.blade.php

			list of users     :
					views/admin/edit.blade.php

			unconfirmed users :
					views/admin/edit.blade.php

			edit users        : 
					views/admin/edit.blade.php

			shows users       :
					views/admin/show.blade.php
			
			auth		  : 
					view genere par laravel