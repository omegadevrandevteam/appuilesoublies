<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::GET('/', 'HomefrontController@index');
Route::resource('admin', 'admin\adminController');
Auth::routes();
Route::GET('adhome','admin\adminController@homeroute');
Route::GET('unconfusers','admin\adminController@usrNotConfirmed');
//Route::POST('/email','admin\adminController@register');
Route::GET('/home', 'HomeController@index');
Route::get('/send_confirm_mail/{id}', [
    'as' => 'send_confirm_mail',
    'uses' => 'admin\adminController@register'
]);

Route::get('/validate/{token}', [
    'as' => 'link_to_edit_logo',
    'uses' => 'users\usersController@update_logo'
]);

//Route::post('/register_users/', [
//    'as' => 'register_users',
//    'uses' => 'admin\adminController@store'
//]);

Route::post('/save_logo/', [
    'as' => 'save_logo',
    'uses' => 'users\usersController@save_logo'
]);
Route::get('send-mail', function () {

    $details = [
        'title' => 'Mail from ItSolutionStuff.com',
        'body' => 'This is for testing email using smtp'
    ];

    \Mail::to('randawilly@hotmail.com')->send(new \App\Mail\sendMail($details));

    dd("Email is Sent.");
});